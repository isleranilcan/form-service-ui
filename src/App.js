import { useEffect, useState } from 'react'
import React from 'react';
import dummydata from "./data.json";
import { Form, Col, Row, Button, InputGroup, FormControl } from 'react-bootstrap'
import Spinner from 'react-bootstrap/Spinner'
import 'react-dropdown-tree-select/dist/styles.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import SectorTree from './SectorTree';
import { notify } from 'react-notify-toast';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';


const assignObjectPaths = (obj, stack) => {
  Object.keys(obj).forEach(k => {
    const node = obj[k];
    if (typeof node === "object") {
      node.path = stack ? `${stack}.${k}` : k;
      assignObjectPaths(node, node.path);
    }
  });
};

const App = () => {

  const [initialData, setInitialData] = useState('');
  const [data, setData] = useState(dummydata);
  const [name, setName] = useState('');
  const [sectors, setSectors] = useState([]);
  const [isTermsAgreed, toggleIsAgreed] = useState(false);
  const [loading, toggleLoading] = useState(false);
  const [sessionId, setSessionId] = useState('');
  const [loadSessionInput, setLoadSessionInput] = useState('');
  const [isLoadToggle, setIsLoadToggle] = useState(false);

  const onSaveButtonClicked = () => {
    toggleLoading(loading => !loading);

    if (sessionId !== '')
      updateRecord({ sessionId, name, sectors, isTermsAgreed })
    else
      saveRecord({ sessionId, name, sectors, isTermsAgreed })
  }

  const onLoadButtonClicked = () => {
    toggleLoading(loading => !loading);

    loadRecord(loadSessionInput)
  }

  function findDFS(objects, id) {
    for (let o of objects || []) {
      if (o.id === id) return o
      const o_ = findDFS(o.children, id)
      if (o_) return o_
    }
  }

  function showValidationMessage(responseData) {
    let text = '';
    Object.values(responseData).forEach(msg => {
      text = text + msg + ', ';
    });
    notify.show(text.slice(0, -2) + ' is mandatory!', 'error', 3500);
  }

  function handleResponseData(responseData) {
    var tempData = JSON.parse(JSON.stringify(initialData));
    responseData.sectors.forEach(itemId => {
      let x = findDFS(tempData, itemId);
      x.checked = true;
    });
    setData(initialData);
    setData(tempData);
    setName(responseData.name);
    setSessionId(responseData.sessionId);
    toggleIsAgreed(responseData.termsAgreed);
    setSectors(responseData.sectors);
    toggleLoading(loading => !loading);
  }

  //LOAD RECORD
  const loadRecord = async (sessionInput) => {
    const requestoptions = {
      method: 'GET',
      headers: {
        'Content-type': 'application/json'
      }
    };

    fetch('http://localhost:8080/api/v1/record?sessionId=' + sessionInput, requestoptions)
      .then(async res => {
        if (!res.ok) {
          return Promise.reject(res.status);
        }
        notify.show('Record is loaded successfully!', 'success', 2500);
        const responseData = await res.json();

        handleResponseData(responseData);

        setIsLoadToggle(false);
      })
      .catch(error => {
        if (error === 404)
          notify.show('Session: ' + sessionInput + ' is not found!', 'error', 3500)
        else
          notify.show('Unexpected error occured while loading record!', 'error', 3500);
        console.error('Error happened: ', error);
        toggleLoading(loading => !loading);
      })
  }

  //UPDATE RECORD
  const updateRecord = async (record) => {
    const requestoptions = {
      method: 'PUT',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(record)
    };

    fetch('http://localhost:8080/api/v1/record', requestoptions)
      .then(async res => {
        if (!res.ok) {
          const responseData = await res.json();
          showValidationMessage(responseData);
          return Promise.reject(res.status);
        }
        notify.show('Record is updated successfully!', 'success', 2500);
        const responseData = await res.json();

        handleResponseData(responseData);
      })
      .catch(error => {
        notify.show('Unexpected error occured while updating record!', 'error', 3500);
        console.error('Record is not updated!', error);
        toggleLoading(loading => !loading);
      })
  }

  //SAVE RECORD
  const saveRecord = async (record) => {
    const requestoptions = {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(record)
    };

    fetch('http://localhost:8080/api/v1/record', requestoptions)
      .then(async res => {
        if (!res.ok) {
          const responseData = await res.json();
          showValidationMessage(responseData);
          return Promise.reject(res.status);
        }
        notify.show('Record is saved successfully!', 'success', 2500);
        const responseData = await res.json();

        handleResponseData(responseData);
      })
      .catch(error => {
        notify.show('Unexpected error occured while saving record!', 'error', 3500);
        console.error('Record is not saved!', error);
        toggleLoading(loading => !loading);
      })
  }

  //INITIAL LOAD OF SECTORs
  useEffect(() => {
    async function fetchSectorData() {
      const requestoptions = {
        method: 'GET',
        headers: {
          'Content-type': 'application/json'
        }
      };

      fetch('http://localhost:8080/api/v1/sectors', requestoptions)
        .then(async res => {
          const data = await res.json();
          if (!res.ok) {
            return Promise.reject(res.status);
          }
          setData(data);
          setInitialData(data);
        })
        .catch(error => {
          console.error('Error happened: ', error);
        })
    }
    fetchSectorData();
  }, []);

  assignObjectPaths(data);

  const onChange = (currentNode, selectedNodes) => {
    setSectors(selectedNodes.map(a => a.id));
  };

  const handleChange = (event) => {
    setIsLoadToggle(event.target.checked);
  };

  const AntSwitch = withStyles((theme) => ({
    root: {
      width: 28,
      height: 16,
      padding: 0,
      display: 'flex',
    },
    switchBase: {
      padding: 2,
      color: theme.palette.grey[500],
      '&$checked': {
        transform: 'translateX(12px)',
        color: theme.palette.grey[500],
        '& + $track': {
          opacity: 1,
          backgroundColor: theme.palette.common.white,
          borderColor: theme.palette.primary.main,
        },
      },
    },
    thumb: {
      width: 12,
      height: 12,
      boxShadow: 'none',
    },
    track: {
      border: `1px solid ${theme.palette.grey[500]}`,
      borderRadius: 16 / 2,
      opacity: 1,
      backgroundColor: theme.palette.common.white,
      borderColor: theme.palette.primary.main,
    },
    checked: {},
  }))(Switch);

  return (
    <div className="my-container">
      <Form.Label>Please enter your name and pick the Sectors you are currently involved in.</Form.Label>
      <br />
      <Form.Label>Session: {sessionId}</Form.Label>

      <InputGroup className="mb-3" >
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Name:</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl type="text"
          value={name} onChange={(e) => setName(e.target.value)}
        />
      </InputGroup>

      <Row>
        <Col>
          <SectorTree data={data} onChange={onChange} />
        </Col>
        <Col>
          <Form.Group>
            <Form.Check label="Agree to terms"
              checked={isTermsAgreed} onChange={(e) => toggleIsAgreed(e.target.checked)}
            />
          </Form.Group>

          <Form.Group>

            <Grid component="label" container alignItems="center" spacing={1} style={{ marginBottom: 15 }}>
              <Grid item>Save</Grid>
              <Grid item>
                <AntSwitch checked={isLoadToggle} onChange={handleChange} name="checkedC" />
              </Grid>
              <Grid item>Load</Grid>
            </Grid>

            {isLoadToggle &&
              <Form.Group>
                <FormControl type="text" style={{ marginBottom: 5 }}
                  value={loadSessionInput} onChange={(e) => setLoadSessionInput(e.target.value)}
                />
                <Button onClick={onLoadButtonClicked} disabled={loading}>
                  {loading && <Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                  />}
                  {loading && <span> Loading...</span>}
                  {!loading && <span>Load</span>}
                </Button>{' '}
              </Form.Group>}

            {!isLoadToggle &&
              <Button onClick={onSaveButtonClicked} disabled={loading}>
                {loading && <Spinner
                  as="span"
                  animation="border"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                />}
                {loading && <span> Saving...</span>}
                {!loading && !sessionId && <span>Save</span>}
                {!loading && sessionId && <span>Update</span>}
              </Button>
            }

          </Form.Group>
        </Col>
      </Row>
    </div >

  );
}

export default App;
